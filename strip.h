// predefinitions for the c compiler ...
void blink1Sec(int ledColor[3]);

// the array that stores the led data for each pixel
int stripData[LED_PIXELS][3];
void initializeStrip() {
  for (int pixel = 0; pixel < LED_PIXELS; pixel++) {
    for (int color = 0; color < 3; color++) {
      stripData[pixel][color] = 0;
    }
  }
}

// predefined color values
int red[3] = {255, 0, 0};
int orange[3] = {255, 64, 0};
int green[3] = {0, 255, 0};
int blue[3] = {0, 0, 255};

// function to write the stored data to the strip
void writeLED() {
  led.clear();
  for (int pixel = 0; pixel < LED_PIXELS; pixel++) {    // for every pixel check for valid values
    if (stripData[pixel][0] < 0) { stripData[pixel][0] = 0; }
    if (stripData[pixel][0] > 255) { stripData[pixel][0] = 255; }
    if (stripData[pixel][1] < 0) { stripData[pixel][1] = 0; }
    if (stripData[pixel][1] > 255) { stripData[pixel][1] = 255; }
    if (stripData[pixel][2] < 0) { stripData[pixel][2] = 0; }
    if (stripData[pixel][2] > 255) { stripData[pixel][2] = 255; }
    // and write the data to the LED strip
    led.setPixelColor(pixel, led.Color(stripData[pixel][0], stripData[pixel][1], stripData[pixel][2]));
  }
  led.show();
}


// functions to symbolize the wlan connection process
int curDotPos = 0;

// initialize the progress bar
void wifiConnectLedStart() {
  curDotPos = 0;
  for (int pixel = 0; (pixel < WLAN_TIMEOUT*2) && (pixel < LED_PIXELS); pixel++) {
    stripData[pixel][0] = blue[0];
    stripData[pixel][1] = blue[1];
    stripData[pixel][2] = blue[2];
  }
  writeLED();
}

// print a pxiel with a dot
void wifiConnectLedPrintDot() {
  if (curDotPos < 0 || curDotPos > LED_PIXELS) {
    return;
  }
  stripData[curDotPos][0] = red[0];
  stripData[curDotPos][1] = red[1];
  stripData[curDotPos][2] = red[2];
  curDotPos++;
  writeLED();
}

// blink red beacuse the timout was hit
void wifiConnectLedTimeout() {
  blink1Sec(red);
}

// display success by blinking green
void wifiConnectLedSuccess() {
  blink1Sec(green);
}


// functions to control the color and pixels of the strip

// set one color for the whole strip
void setStripColor(int ledColor[3]) {
  for (int pixel = 0; pixel < LED_PIXELS; pixel++) {
    stripData[pixel][0] = ledColor[0];
    stripData[pixel][1] = ledColor[1];
    stripData[pixel][2] = ledColor[2];
  }
  writeLED();
}

// set an interval of pixels
void setIntervalColor(int startPixel, int endPixel, int ledColor[3]) {
  if (startPixel <= endPixel) {
    for (int pixel = startPixel; (pixel <= endPixel) && (pixel < LED_PIXELS); pixel++) {
      stripData[pixel][0] = ledColor[0];
      stripData[pixel][1] = ledColor[1];
      stripData[pixel][2] = ledColor[2];
    }
  }
  writeLED();
}


// functions for rainbow patterns

// fills whole strip with rainbow starting by offset hue value at pixel 0
void fillRainbowStrip(int offset) {
  int hue = offset;
  int step = (int) 360/LED_PIXELS;
  for (int pixel = 0; pixel < LED_PIXELS; pixel++) {
    int r = 0;
    int g = 0;
    int b = 0;
    convertHueToRGB(hue, &r, &g, &b);
    stripData[pixel][0] = r;
    stripData[pixel][1] = g;
    stripData[pixel][2] = b;
    hue = hue + step;
    if (hue >= 360) {
      hue = 0;
    }
  }
  writeLED();
}

// rainbow animation
int rainbowOffset = 0;
bool animateRainbow = false;
int rainbowSpeed = 1;
void startRainbowAnimtation() {
  animateRainbow = true;
}
void progressRainbowAnimation() {
  rainbowOffset = rainbowOffset + rainbowSpeed;
  if (rainbowOffset >= 360) {
    rainbowOffset = 0;
  }
  fillRainbowStrip(rainbowOffset);
}
void stopRainbowAnimation() {
  animateRainbow = false;
}


// do 6 blocks with rainbow colors
void do6BlockRainbow() {
  int step = LED_PIXELS/6;
  int color[6][3] = {
    {1,0,0},
    {1,1,0},
    {0,1,0},
    {0,1,1},
    {0,0,1},
    {1,0,1}
  };
  for (int c = 0; c < 6; c++) {
    for (int pixel = c*step; pixel < (c+1)*step; pixel++) {
      stripData[pixel][0] = color[c][0] * 255;
      stripData[pixel][1] = color[c][1] * 255;
      stripData[pixel][2] = color[c][2] * 255;
    }
  }
  for (int pixel = step*6; pixel < LED_PIXELS; pixel++) {
    stripData[pixel][0] = color[5][0] * 255;
    stripData[pixel][1] = color[5][1] * 255;
    stripData[pixel][2] = color[5][2] * 255;
  }
  writeLED();
}




// led helper function to blink 1 times, takes 1 second
void blink1Sec(int ledColor[3]) {
  setStripColor(ledColor);
  delay(500);
  led.clear();
  led.show();
  delay(500);
  setStripColor(ledColor);
}
