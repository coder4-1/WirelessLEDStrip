#include <WiFiClient.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>
#include <string.h>
#include <Regexp.h>
#include "settings.h"
#include "helper.h"

// instantiation of the used libarys
WiFiClient wlanClient;
PubSubClient mqttClient(wlanClient);
Adafruit_NeoPixel led(LED_PIXELS, LED_PIN, NEO_GRB + NEO_KHZ800);
MatchState ms;

// load extra files
#include "strip.h"

// global variables
bool offline = true;
char MATCH_HEX_COLOR[] = "^#[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]"; //"^#[0-9a-fA-F]{6}$" 
char MATCH_RAINBOW[] = "^rainbowcolor";
char MATCH_RAINBOW_OFFSET[] = "^rainbow[0-9]+";
int RAINBOW_OFFSET_PREFIX_LENGTH = 7;
char MATCH_RAINBOW_ANIMATION_GO[] = "^rainbow#go";
char MATCH_RAINBOW_ANIMATION_STOP[] = "^rainbow#stop";
char MATCH_RAINBOW_ANIMATION_SPEED[] = "^rainbow#speed[0-9]+";
char MATCH_6_BLOCK_RAINBOW[] = "^6brainbow";
int RAINBOW_SPEED_PREFIX_LENGTH = 13;
char MATCH_CUSTOM_PIXELS[] = "^pixel[0-9][0-9][0-9]to[0-9][0-9][0-9]#[0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F][0-9a-fA-F]";
int CUSTOM_PIXEL_COLOR_PREFIX_LENGTH = 14;
int CUSTOM_PIXEL_START_PREFIX_LENGTH = 5;
int CUSTOM_PIXEL_END_PREFIX_LENGTH = 10;
int tickSpeed = 100;    // milliseconds after the next animation ticks are executed
long lastTick = 0;   // milliseconds from millis() function when the last tick was executed


void setup() {

  // LED start initialisation light value
  led.begin();
  initializeStrip();

  Serial.begin(115200);
  if (connectWLAN()) {    // if the wifi connection is successful, start the mqtt connection
    connectMQTT();
    offline = false;
  } else {
    // do something when offline, all blue
    for (int i=0; i < LED_PIXELS; i++) {
      led.setPixelColor(i, led.Color(orange[0],orange[1],orange[2]));
    }
    led.show();
  }

}

void loop() {
  if (!mqttClient.connected() && !offline) {    // connect to the mqtt broker if the connection is lost
    reconnect();
  }
  mqttClient.loop();    // reveice subscribed mqtt topics in the callback function

  // execute a tick for the animations
  long curMillis = millis();
  if (curMillis - lastTick > tickSpeed) {    // a tick must be executed
    lastTick = curMillis;
    
    // do animations
    if (animateRainbow) {
      progressRainbowAnimation();
    }
    
  }

}


// functions for handeling wifi and mqtt connections

bool connectWLAN() {
  Serial.println();
  Serial.print("Connecting to WLAN: ");
  Serial.println(WLAN_SSID);

  WiFi.disconnect();
  WiFi.hostname(WLAN_HOSTNAME);
  WiFi.begin(WLAN_SSID, WLAN_PASS);

  // print mac address
  Serial.print("With Mac-Address: ");
  Serial.println(WiFi.macAddress());

  long startTime = millis();
  writeLED();   // display the connection process with LEDs
  wifiConnectLedStart();
  while((WiFi.status() != WL_CONNECTED)) {
    delay(500);
    Serial.print("Trying to connect with Mac-Address: ");
    Serial.println(WiFi.macAddress());
    wifiConnectLedPrintDot();   // display the connection process with LEDs
    if ((millis()-startTime)/1000 >= WLAN_TIMEOUT) {   // if connection is timed out close every wifi component
      Serial.println();
      Serial.println("WLAN connect timed out!");
      wifiConnectLedTimeout();   // display the connection process with LEDs
      WiFi.disconnect();
      return false;
    }
  }

  Serial.println();   // print wifi parameters when connection was successful
  Serial.print("WiFi Connected to SSID: ");
  Serial.println(WLAN_SSID);
  Serial.print("IP-Address: ");
  Serial.println(WiFi.localIP());
  Serial.print("Mac-Address: ");
  Serial.println(WiFi.macAddress());
  Serial.println();
  Serial.println();
  wifiConnectLedSuccess();   // display the connection process with LEDs
  return true;
}


void connectMQTT() {
  mqttClient.setServer(MQTT_BROKER, MQTT_PORT);
  mqttClient.setCallback(callback);
}


void reconnect() {
  while(!mqttClient.connected()) {    // if the mqtt connection is lost
    Serial.println("Connecting to MQTT...");
    if (mqttClient.connect(MQTT_CLIENT_NAME, MQTT_USER, MQTT_PASS)) {   // try to connect again
      Serial.println("connected!");
      // publish some starting message
      mqttClient.publish("broadcast", MQTT_CLIENT_NAME " successfully started!");
      // subscribe to the needed topics
      mqttClient.subscribe(MQTT_TOPIC_NAME);
    } else {
      Serial.print("Error: ");
      Serial.println(mqttClient.state());
      Serial.println("Failed to connect. Trying again in 5 seconds");
      delay(5000);
    }
  }
}


void callback(char* topic, byte* payload, unsigned int length) {
  char message[length];
  Serial.print("Message received [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    message[i] = (char)payload[i];
  }
  Serial.println();

  // match the incomming messages
  ms.Target(message);
  if (1 == ms.Match(MATCH_HEX_COLOR)) {   // hex color strings
    int r = 0;
    int g = 0;
    int b = 0;
    getRGBFromHexString(&r, &g, &b, message, 1);
    int color[3] = {r, g, b};
    setStripColor(color);
  }
  if (1 == ms.Match(MATCH_RAINBOW)) {   // rainbow without offset or animation
    fillRainbowStrip(0);
  }
  if (1 == ms.Match(MATCH_RAINBOW_OFFSET)) {
    int offset = getIntFromPrefixString(message, length, RAINBOW_OFFSET_PREFIX_LENGTH);
    if (offset > 360) {
      offset = offset % 360;
    }
    rainbowOffset = offset;
    fillRainbowStrip(offset);
  }
  if (1 == ms.Match(MATCH_RAINBOW_ANIMATION_GO)) {   // rainbow animation go
    startRainbowAnimtation();
  }
  if (1 == ms.Match(MATCH_RAINBOW_ANIMATION_STOP)) {   // rainbow animation stop
    stopRainbowAnimation();
  }
  if (1 == ms.Match(MATCH_RAINBOW_ANIMATION_SPEED)) {   // rainbow animation speed
    int speed = getIntFromPrefixString(message, length, RAINBOW_SPEED_PREFIX_LENGTH);
    if (speed > 360) {
      speed = 360;
    }
    rainbowSpeed = speed;
  }
  if (1 == ms.Match(MATCH_6_BLOCK_RAINBOW)) {
    do6BlockRainbow();
  }
  if (1 == ms.Match(MATCH_CUSTOM_PIXELS)) {
    int r = 0;
    int g = 0;
    int b = 0;
    getRGBFromHexString(&r, &g, &b, message, CUSTOM_PIXEL_COLOR_PREFIX_LENGTH);
    int color[3] = {r, g, b};
    int startPixel = getIntFromPrefixString(message, CUSTOM_PIXEL_START_PREFIX_LENGTH+3, CUSTOM_PIXEL_START_PREFIX_LENGTH);
    int endPixel = getIntFromPrefixString(message, CUSTOM_PIXEL_END_PREFIX_LENGTH+3, CUSTOM_PIXEL_END_PREFIX_LENGTH);
    setIntervalColor(startPixel, endPixel, color);
  }

}
