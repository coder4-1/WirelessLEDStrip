// converts a Hex Digit to it's decimal value
int getHexDigitVal(char d) {
  int val = -1;
  switch(d) {
    case 'F':
    case 'f':
      val = 15;
      break;
    case 'E':
    case 'e':
      val = 14;
      break;
    case 'D':
    case 'd':
      val = 13;
      break;
    case 'C':
    case 'c':
      val = 12;
      break;
    case 'B':
    case 'b':
      val = 11;
      break;
    case 'A':
    case 'a':
      val = 10;
      break;
    default:
      val = (int) d - 48;
      break;
  }
  return val;
}


// helper to convert hex to decimal
// can only convert 2 hex digits to int between 0 and 255
int convertHexToDec(char d1, char d2) {
  return getHexDigitVal(d1)*16 + getHexDigitVal(d2);
}


// helper to convert hue color to rgb color
void convertHueToRGB(int hue, int *r, int *g, int *b) {
  double splitter = (double)hue/(double)60;
  double r1 = 0.0;
  double g1 = 0.0;
  double b1 = 0.0;
  if (0 <= splitter && splitter < 1) {
    double x = splitter;
    r1 = 1;
    g1 = x;
    b1 = 0;
  } else if (1 <= splitter && splitter < 2) {
    double x = abs(splitter - 2);
    r1 = x;
    g1 = 1;
    b1 = 0;
  } else if (2 <= splitter && splitter < 3) {
    double x = splitter - 2;
    r1 = 0;
    g1 = 1;
    b1 = x;
  } else if (3 <= splitter && splitter < 4) {
    double x = abs(splitter - 4);
    r1 = 0;
    g1 = x;
    b1 = 1;
  } else if (4 <= splitter && splitter < 5) {
    double x = splitter - 4;
    r1 = x;
    g1 = 0;
    b1 = 1;
  } else if (5 <= splitter && splitter < 6) {
    double x = abs(splitter - 6);
    r1 = 1;
    g1 = 0;
    b1 = x;
  }
  *r = (int) (r1 * 255);
  *g = (int) (g1 * 255);
  *b = (int) (b1 * 255);
}


// get a interger number of an string with prefix
int getIntFromPrefixString(char* msg, int msgLength, int prefixLength) {
  int val = 0;
  int numberLength = msgLength - prefixLength;
  for (int numberIndex = 0; numberIndex < numberLength; numberIndex++) {
    int exponent = (numberLength-1) - numberIndex;
    int valueAtPos = (int) msg[numberIndex + prefixLength] - 48;
    val = val + valueAtPos * pow(10, exponent);
  }
  return val;
}


// get a hex color out of an string with prefix
void getRGBFromHexString(int *r, int *g, int *b, char* msg, int prefixLength) {
  *r = convertHexToDec(msg[0+prefixLength], msg[1+prefixLength]);
  *g = convertHexToDec(msg[2+prefixLength], msg[3+prefixLength]);
  *b = convertHexToDec(msg[4+prefixLength], msg[5+prefixLength]);
}
